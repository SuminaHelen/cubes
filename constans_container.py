from PyQt5.QtGui import QColor, QPixmap
from PyQt5.QtCore import Qt


class Constants(object):
    def __init__(self):
        self.width = 15
        self.height = 15
        self.max_colors = 4
        self.green_red = QPixmap('pics\\greenred.png')
        self.yellow_purple = QPixmap('pics\\yellowpurple.png')
        self.green_red_light = QPixmap('pics\\greenred_ligth.png')
        self.yellow_purple_light = QPixmap('pics\\yellowpurple_light.png')
        self.color_table = [QColor(0, 0, 0, 0), QColor(34, 139, 34, 255), QColor(255, 0, 0, 255),
                            QColor(255, 215, 0, 255), QColor(218, 112, 214, 255),
                            self.green_red, self.yellow_purple]
        self.light_colors = [QColor(0, 0, 0, 0),  QColor(34, 139, 34, 100), QColor(255, 0, 0, 100),
                             QColor(255, 215, 0, 100), QColor(218, 112, 214, 100),
                             self.green_red_light, self.yellow_purple_light]
        self.cube_size = 30
        self.indent = 20
        self.window_geometry = 300, 100, 650, 500
        self.window_title = 'Kubiki'
        self.icon_name = 'pics\\kub.png'
        self.button_title = "RESTART GAME"
        self.button_geometry = 520, 25
        self.total_scores = self.width * self.height
        self.scores_color = QColor("black")
        self.scores_font = ('Decorative', 14, 50)
        self.scores_geometry = (540, 100, 100, 100, Qt.AlignBottom)
        self.win_pic = 'pics\\youwin.png'
        self.lose_pic = 'pics\\youlose.png'
        self.extra_text = "You can remove \n colors at ones! \n green + red \n yellow + violet"
        self.extra_text_geometry = (490, 150, 200, 200, Qt.AlignBottom)
        self.extra_first_cube_geometry = 0
