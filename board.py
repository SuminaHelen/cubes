from PyQt5.QtWidgets import QApplication, QWidget, QPushButton, QLabel
from PyQt5.QtGui import QPainter, QIcon, QFont, QPixmap, QColor
from game import Game 
import sys
from PyQt5 import QtCore
from constans_container import Constants


class Board(QWidget):
    def __init__(self, game, color_table, cube_size, indent, const):
        super().__init__()
        self.game = game
        self.color_table = color_table
        self.cube_size = cube_size
        self.indent = indent
        self.const = const
        self.label = QLabel(self)
        self.game_over = False
        self.light_cubes = set()
        self.init_ui()

    def init_ui(self):
        """
        initializes game window with different widgets on it
        """
        self.setGeometry(*self.const.window_geometry)
        self.setWindowTitle(self.const.window_title)
        self.setWindowIcon(QIcon(self.const.icon_name))
        btn = QPushButton(self.const.button_title, self)
        btn.move(*self.const.button_geometry)
        btn.clicked.connect(self.button_clicked)
        self.setMouseTracking(True)
        self.show()

    def button_clicked(self):
        """
        checks if the button was clicked
        """
        self.game = Game.create_random(self.const.width, self.const.height, self.const.max_colors)
        self.game_over = False
        self.repaint()

    def paintEvent(self, e):
        """
        sets going drawing methods
        """
        qp = QPainter()
        qp.begin(self)
        self.draw_squares(qp)
        self.draw_extra_task(qp)
        self.draw_scores(qp)
        self.draw_game_over(qp)
        qp.end()

    def draw_squares(self, qp):
        """
        draws cubes with corresponding colors
        """
        for x in range(self.game.width):
            for y in range(self.game.height):
                if (x, y) in self.light_cubes:
                        color = self.const.light_colors[self.game.field[x][y]]
                else:
                        color = self.color_table[self.game.field[x][y]]
                if 5 <= self.game.field[x][y] <= self.const.max_colors + 2:
                    qp.drawPixmap(self.indent + y * self.cube_size, self.indent + x * self.cube_size,
                                  color)
                    self.label.show()
                    color = QColor(0, 0, 0, 0)
                qp.setBrush(color)
                qp.drawRect(self.indent + y * self.cube_size, self.indent + x * self.cube_size,
                            self.cube_size, self.cube_size)

    def draw_scores(self, qp):
        """
        draws scores and shows if the game was won
        """
        self.scores = Game.count_points(self.game)
        text = "{}/{}".format(self.scores, self.const.total_scores)
        qp.setPen(self.const.scores_color)
        qp.setFont(QFont(*self.const.scores_font))
        qp.drawText(*self.const.scores_geometry, text)
        if self.scores == self.const.total_scores:
            pixmap = QPixmap(self.const.win_pic)
            qp.drawPixmap(0, 0, pixmap)
            self.label.show()

    def draw_game_over(self, qp):
        """
        draws a picture if the game was lost
        """
        if self.game_over:
            pixmap = QPixmap(self.const.lose_pic)
            qp.drawPixmap(0, 0, pixmap)
            self.label.show()

    def draw_extra_task(self, qp):
        """
        writes which cube's colors a player can remove at once
        """
        text = self.const.extra_text
        qp.setPen(self.const.scores_color)
        qp.setFont(QFont(*self.const.scores_font))
        qp.drawText(*self.const.extra_text_geometry, text)

    def mousePressEvent(self, e):
        """
        some cubes are removed if the mouse button was pressed
        """
        x, y = e.pos().x(), e.pos().y()
        norm_x = (x - self.indent) // self.cube_size
        norm_y = (y - self.indent) // self.cube_size
        if 0 <= norm_x < self.game.width and 0 <= norm_y < self.game.height:
            Game.make_empty(self.game, Game.click(self.game, norm_y, norm_x, True))
            self.game.field = Game.normalize(self.game)
            self.scores = Game.count_points(self.game)
            if self.scores != 225 and Game.check_progress(self.game):
                self.game_over = True
            self.light_cubes = set()
            QWidget.update(self)

    def mouseMoveEvent(self, e):
        """
        lightning cubes with the same color and a common side when cursor is moving
        """
        if e.buttons() == QtCore.Qt.NoButton:
            x, y = e.pos().x(), e.pos().y()
            norm_x = (x - self.indent) // self.cube_size
            norm_y = (y - self.indent) // self.cube_size
            if 0 <= norm_x < self.game.width and 0 <= norm_y < self.game.height \
                    and self.game.field[norm_y][norm_x]:
                self.light_cubes = Game.click(self.game, norm_y, norm_x, True)
                QWidget.update(self)


def create_board(const):
    """
    creates game field, initializes class Board
    """
    g = Game.create_random(const.width, const.height, const.max_colors)
    board = Board(g, const.color_table, const.cube_size, const.indent, const)
    return board


if __name__ == '__main__':
    app = QApplication(sys.argv)
    c = Constants()
    b = create_board(c)
    sys.exit(app.exec_())
