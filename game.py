import random


class Game(object):
    def __init__(self, field):
        self.width = len(field)
        self.height = len(field[0])
        self.field = field
        self.points = 0

    def click(self, x, y, extra_task=False):
        """
        returns points of cubes with the same color and common side using DFS
        """
        stack = [(x, y)]
        removed = set()
        relatable_colors = [[], [1, 5], [2, 5], [3, 6], [4, 6], [1, 2, 5], [3, 4, 6]]
        rcolors = relatable_colors[self.field[x][y]]
        #square_sides = 0
        while stack:
            #square_sides += 1
            nx, ny = stack.pop()
            is_in_bounds = 0 <= nx < self.width and 0 <= ny < self.height
            if not is_in_bounds or (nx, ny) in removed or self.field[nx][ny] not in rcolors:
                continue
            removed.add((nx, ny))
            stack.append((nx + 1, ny))
            stack.append((nx - 1, ny))
            stack.append((nx, ny + 1))
            stack.append((nx, ny - 1))
        #if square_sides == 5:
        #    return set()
        return removed if len(removed) > 1 else set()

    def make_empty(self, removed):
        """
        makes cubes white
        """
        for rx, ry in removed:
            self.field[rx][ry] = 0

    def normalize(self):
        """
        takes away spaces on field; moves cubes down and left
        """
        trnsp_field = list(map(list, zip(*self.field)))
        empty_columns = []
        for i in range(len(trnsp_field)):
            new_column = []
            for x in filter(bool, trnsp_field[i]):
                new_column.append(x)
            if not new_column:
                empty_columns.append(i)
            else:
                pad = [0] * (self.width - len(new_column))
                trnsp_field[i] = pad + new_column
        empty_columns.reverse()
        for index in empty_columns:
            del trnsp_field[index]
        pad_len = (self.width - len(trnsp_field))
        trnsp_field += [[0] * self.width for _ in range(pad_len)]
        return list(map(list, zip(*trnsp_field)))

    def count_points(self):
        """
        counts point for every removed cube
        """
        self.points = 0
        for x in range(self.width):
            for y in range(self.height):
                if self.field[x][y] == 0:
                    self.points += 1
        return self.points

    def check_progress(self):
        """
        checks every cube if there is a possibility to take away at least some of them
        """
        game_over = True

        def is_in_bounds(nx, ny):
            """
            defines if the point is inside field
            """
            return 0 <= nx < self.width and 0 <= ny < self.height

        for x in range(self.width):
            if not game_over:
                break
            for y in range(self.height):
                state = self.field[x][y]
                if state == 0:
                    break
                elif (is_in_bounds(x - 1, y) and self.field[x - 1][y] == state) or \
                        (is_in_bounds(x + 1, y) and self.field[x + 1][y] == state) or \
                        (is_in_bounds(x, y + 1) and self.field[x][y + 1] == state) or \
                        (is_in_bounds(x, y - 1) and self.field[x][y - 1] == state):
                    game_over = False
                    break
        return game_over

    @staticmethod
    def create_random(width, height, max_colors):
        """
        creates a field with random colored cubes on it
        """
        field = [[random.randint(1, max_colors) for _ in range(height)] for _ in range(width)]
        for i in range(height//2):
            x = random.randint(0, height-1)
            y = random.randint(0, width-1)
            field[x][y] = random.choice([5, 6])
        return Game(field)
