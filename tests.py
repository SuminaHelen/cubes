import unittest
from game import Game


class TestGame(unittest.TestCase):
    def test_click_2x2(self):
        game = Game([[1, 1], [2, 2]])
        game.make_empty(game.click(1, 1))
        self.assertEqual([[1, 1], [0, 0]], game.field)

    def test_click_3x3(self):
        game = Game([[1, 2, 2], [2, 2, 2], [3, 2, 3]])
        game.make_empty(game.click(1, 1))
        self.assertEqual([[1, 0, 0], [0, 0, 0], [3, 0, 3]], game.field)

    def test_click_one_cell(self):
        game = Game([[1]])
        game.make_empty(game.click(0, 0))
        self.assertEqual(1, game.field[0][0])

    def test_click_alone_cell(self):
        game = Game([[1, 1], [1, 2]])
        game.make_empty(game.click(1, 1))
        self.assertEqual([[1, 1], [1, 2]], game.field)

    def test_normalization(self):
        game = Game([[2, 1], [1, 1]])
        game.make_empty(game.click(1, 1))
        self.assertEqual([[0, 0], [2, 0]], game.normalize())

    def test_normalization2(self):
        game = Game([[1, 2, 2], [2, 2, 2], [3, 2, 3]])
        game.make_empty(game.click(1, 1))
        self.assertEqual([[0, 0, 0], [1, 0, 0], [3, 3, 0]], game.normalize())

    def test_alone_cell_normalization(self):
        game = Game([[2, 1], [1, 1]])
        game.make_empty(game.click(0, 0))
        self.assertEqual([[2, 1], [1, 1]], game.normalize())

    def test_scoring(self):
        game = Game([[2, 1], [1, 1]])
        self.assertEqual(0, game.count_points())

    def test_scoring_with_zeros(self):
        game = Game([[2, 1], [1, 1]])
        game.make_empty(game.click(1, 0))
        self.assertEqual(3, game.count_points())

    def test_if_game_is_over(self):
        game = Game([[2, 1], [1, 1]])
        self.assertEqual(0, game.check_progress())

    def test_if_game_is_over2(self):
        game = Game([[2, 1], [1, 1]])
        game.make_empty(game.click(1, 1))
        self.assertEqual(1, game.check_progress())

    def test_mouse_move(self):
        game = Game([[3, 2, 1],
                     [3, 1, 1],
                     [3, 2, 2]])
        self.assertEqual([{(2, 0), (1, 0), (0, 0)}], [game.click(0, 0)])
        self.assertEqual(set(), game.click(0, 1))
        self.assertEqual([{(0, 2), (1, 2), (1, 1)}], [game.click(0, 2)])
        self.assertEqual([{(2, 1), (2, 2)}], [game.click(2, 2)])

    def test_extra_task(self):
        game = Game([[1, 1], [2, 5]])
        game.make_empty(game.click(0, 0, True))
        self.assertEqual([[0, 0], [2, 0]], game.field)
        game = Game([[1, 1], [2, 5]])
        game.make_empty(game.click(1, 1, True))
        self.assertEqual([[0, 0], [0, 0]], game.field)

    def test_extra_task2(self):
        game = Game([[1, 1], [3, 3]])
        game.make_empty(game.click(1, 0, True))
        self.assertEqual([[1, 1], [0, 0]], game.field)


if __name__ == '__main__':
    unittest.main()
